const mongoose = require('mongoose');


mongoose.Promise = global.Promise;

mongoose.connect(process.env.DATABASE, {
  useUnifiedTopology: true,
  useCreateIndex: true,
  useNewUrlParser: true,
})
  .then(() => (
    
    console.log('DB connection was successfully!!')))
  .catch((err) => {
    console.log(`DB Connection Error: ${err.message}`);
    process.exit();
  });
