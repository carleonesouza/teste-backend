'use strict';

function authUser(req, res, next){
    if(req.body.email == null){
        res.status(403)
        return res.send({menssage: '403 Forbidden'});
    }
    next();
}

function authRole(role){
    return (req, res, next) =>{
        if(req.session.role !== role){
            res.status(401).send({menssage: 'Role 401 Unauthorized'})
        }
        next();
    }
}

module.exports = {authUser, authRole }