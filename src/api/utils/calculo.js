"use strict";

const Vote = require("../models/vote.model");

function avarageResult(votes = [Vote]){ 
  const media = votes.reduce((soma, vote) => soma += vote.movieNote, 0) / votes.length;
  return media;
}

module.exports = {
  avarageResult,
};
