const jwt = require('jsonwebtoken');

exports.checkAuth = (req, res, next) => {
    if (!req.headers.authorization || !req.headers.authorization.startsWith("Bearer ")) {
        res.status(401).json({ message: "Unauthorized" }).end();
        return;
    }

    const idToken = req.headers.authorization.split('Bearer ')[1];
    jwt.verify(idToken, process.env.JWT_KEY, (error, decoded) => {
                if (error) {
                    res.status(400).json({ message: "Invalid Token "+error });
                    return;
                } else {
                    req.user = decoded
                    next();
                }
            });
};


exports.isNullOrWhiteSpace = (input) => {
    if (typeof input === 'undefined' || input == null || input == '') return true;
    return (typeof input === 'string') ? input.replace(/\s/g, '').length < 1 : false;
};
