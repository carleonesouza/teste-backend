const mongoose = require("mongoose");

const { Schema } = mongoose;

const filmeSchema = new Schema(
    {
        nome:
        {
            type: String,
            require: true,
            unique: true
            
        },
        genero: 
        {
            type: String,
            require: true
        },
        diretor:{
            type: String,
            require: true
        },
        atores: [
            {
                type: String
            
            }
        ],
        descricao: 
        {
            type: String,
            require: true,
            max: 255,
            min: 6
        },
        duracao:
        {
            type: String,
            require: true
        },
        classificacao: 
        {
            type: String,
            require: true
        },

    }
    ,
{
  locale: { String },
},
{
  timestamps: true,
  collation: 'filmes',

}
);

module.exports = mongoose.model("Filme", filmeSchema);