const mongoose = require("mongoose");
const User = require("./user.model");
const Filme = require("./filme.model");

const { Schema } = mongoose;

const voteSchema = new Schema(
  {
    filme: { type: String},
    user:  { type: String},
    movieNote: {
      type: Number,
      require: true,
    },
  },
  {
    locale: { String },
  },
  {
    timestamps: true,
    collation: "votes",
  }
);
voteSchema.pre("save", async function (next) {
  var vote = this;

  try {
    
    const filme =  await Filme.findOne({'nome': vote.filme})
    .exec()
    .then((result =>  { return result }))
    .catch((err) => {return err});

    const user = await User.findOne({'email':vote.user}) 
    .exec()
    .then((result) =>  { return result})
    .catch((err) => { return err});
    
      vote.filme = filme._id;
      vote.user = user._id
 
    next()
   
  } catch (error) {
    next(error);
  }
});
module.exports = mongoose.model("Vote", voteSchema);
