const mongoose = require("mongoose");

const { Schema } = mongoose;

const userSeralizationSchema = new Schema(
  {
    fullName: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      required: true,
      unique: true,
      match: /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/,
    },
    role: {
      type: String,
      require: true,
    },
  },
);


module.exports = mongoose.model("UserSeralization", userSeralizationSchema);