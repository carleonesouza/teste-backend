const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

const { Schema } = mongoose;

const userSchema = new Schema(
  {
    fullName: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      required: true,
      unique: true,
      match: /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/,
    },
    token: {
      type: String,
      require: true,
      expiresIn: 1200
    },
    password: {
      type: String,
      required: true,
    },
    role: {
      type: String,
      require: true,
    },
  },
  {
    locale: { String },
  },
  {
    timestamps: true,
    collation: "users",
  }
);

userSchema.pre("save", async function (next) {
  var user = this;

  try {
    const token = jwt.sign(
      { email: user.email, user: user.password },
      process.env.JWT_KEY,
      { expiresIn: 1200 }
    );
    user.token = token;

    const salt = await bcrypt.genSalt(process.env.SALT_WORK_FACTOR);
    const hashedPass = await bcrypt.hash(user.password, salt);
    user.password = hashedPass;
    next();
  } catch (error) {
    next(error);
  }
});

module.exports = mongoose.model("User", userSchema);
