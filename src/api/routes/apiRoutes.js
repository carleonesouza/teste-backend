const Router = require('express');
const bodyParser = require('body-parser');
const router = Router();
const userController = require('../controllers/userController');

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({extended: false }));
router.use(bodyParser.json({ type: 'application/vnd.api+json' }));



router.post('/users/login', userController.login)
router.post('/users/register', userController.createUser);
router.post('/users/check', userController.check)
router.post('/users/logout', userController.logout)

module.exports = router;
