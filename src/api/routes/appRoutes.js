const router = require('express-promise-router')();
const bodyParser = require('body-parser');

const apiController = require('../controllers/apiController');


router.use(bodyParser.json());
router.use(bodyParser.urlencoded({extended:false}));
router.use(bodyParser.json({ type: 'application/vnd.api+json' }));

router.get('/users', apiController.findAllUsers);
router.get('/users/:id', apiController.findUserById);

router.get('/movies', apiController.listMovies);
router.get('/votes', apiController.listVotes);
router.get('/movies/:id', apiController.detailsMovie)

router.post('/movies', apiController.createMovie);
router.post('/movies/vote', apiController.voteMovie);

//router.put('/movies/:id', apiController.updateMovie);
router.patch('/movies/:id', apiController.updateMovie);
router.patch('/users/:id', apiController.updateUser);

router.delete('/users/:id', apiController.deleteUser);
router.delete('/movies/:id', apiController.deleteMovie);




module.exports = router;
