const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const User = require("../models/user.model");

exports.createUser = async (req, res) => {
  req.session.role = req.body.role;
  User.findOne({ email: req.body.email })
    .exec()
    .then((user) => {
      if (user) {
        return res.status(409).json({
          message: "Mail exists",
        });
      } else {
        const user = new User(req.body);
        const newUser = user
          .save()
          .then((usr) => { console.log('User Created Successfully!'); return usr })
          .catch((err) => {
            res.status(400).json({ message: " " + err });
          });
          res.status(201).send(newUser);
      }
    });
};

exports.authenticateUser = async (req, res) => {
  return User.findOne({ email: req.body.email })
    .exec()
    .then((user) => {
      if (!user) {
        return res.status(401).json({
          message: "Auth failed",
        });
      }
      bcrypt.compare(req.body.password, user.password, (err, result) => {
        if (err) {
          return res.status(401).json({
            message: "Auth failed",
          });
        }

        if (result && req.body.accessToken === user.accessToken) {
          const token = jwt.sign(
            {
              email: user.email,
              userId: user._id,
            },
            process.env.JWT_KEY,
            {
              expiresIn: "1h",
            }
          );
          user.token = token;
          res.status(200).json(token);
        } else {
          res.status(401).json({ message: "Auth failed" });
        }
      });
    })
    .catch((err) => {
      res.status(500).json({
        message: err,
      });
    });
};

exports.login = async (req, res) => {
  const token = jwt.sign(
    { email: req.body.email, userId: req.body.password }, process.env.JWT_KEY, { expiresIn: 3600 });

  userSession = await User.updateOne(
    { email: req.body.email },
    {
      token: token,
    }
  )
    .then(() => console.log("Auth Successfully"))
    .catch((err) => {
      res.status(500).json({
        message: " " + err,
      });
    });

  return User.findOne({ email: req.body.email })
    .exec()
    .then((user) => {
      if (!user) {
        return res.status(401).json({
          message: "Auth failed",
        });
      } else {
        bcrypt.compare(req.body.password, user.password, (err, result) => {
          if (err) {
            return res.status(401).json({
              message: "Auth failed",
            });
          } else {
            req.session.role = user.role
            return res.status(200).json(user);
          }
        });
      }
    })
    .catch((err) => {
      res.status(500).json({
        message: "" + err,
      });
    });
};

exports.logout = (req, res) => {
  req.session.destroy(function(err) {
    console.log(err);
  })
  User.updateOne({ email: req.body.user.email }, { accessToken: null }, () => {
    res
      .status(200)
      .json({
        id: req.trackId,
        status: 200,
        success: true,
        message: "See you later!",
      })
      .end();
  }).catch(() => {
    res
      .status(500)
      .json({
        id: req.trackId,
        status: 500,
        success: false,
        message: "Sorry, an error ocurred while processing your request",
      })
      .end();
    return;
  });
};

exports.check = async (req, res) => {
  const user = await User.findOne({ email: req.body.user.email }).exec();
  res.status(200).send(user);
};
