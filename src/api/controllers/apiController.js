const User = require("../models/user.model");
const UserSeralization = require("../models/userSeralization");
const Filme = require("../models/filme.model");
const Vote = require("../models/vote.model");
const { avarageResult } = require("../utils/calculo");
const role = require("../utils/roles");
const { query } = require("express");

//Find Users
exports.findAllUsers = async (req, res) => {

  console.log(req.session);
  if (req.session.role !== role.ADMIN) {
    return res.status(401).send({ menssage: "401 Unauthorized" });
  } else {
    const users = await User.find({});
    const arrayUsers = [];
    users.map((user) => {
      arrayUsers.push(new UserSeralization(user));
    });
    return res.status(200).send(arrayUsers);
  }
};

//Find User by Id
exports.findUserById = async (req, res) => {
  if (req.session.role !== role.ADMIN) {
    return res.status(401).send({ menssage: "401 Unauthorized" });
  } else {
    const user = await User.findOne({ _id: req.params.id });
    const userSeralization = new UserSeralization(user);
    return res.status(200).send(userSeralization);
  }
};

//Delete a User by id
exports.deleteUser = async (req, res) => {
  if (req.session.role !== role.ADMIN) {
    return res.status(401).send({ menssage: "401 Unauthorized" });
  } else {
    await User.findByIdAndDelete({ _id: req.params.id })
      .then(() => {
        console.log("User was DELETE Successfully!");
        return res
          .status(200)
          .send({ message: "User was DELETE Successfully!" });
      })
      .catch((err) => {
        return res.status(500).json({
          error: err,
        });
      });
  }
};

//Register a Movie
exports.createMovie = async (req, res) => {
  Filme.findOne({ nome: req.body.nome })
    .exec()
    .then((filme) => {
      if (filme) {
        return res.status(409).json({
          message: "Movie Already Registered!",
        });
      } else {
        const filme = new Filme(req.body);
        filme
          .save()
          .then((flm) => {
            console.log("Movie Save Successfully!");
            return res.status(201).send(flm);
          })
          .catch((err) => {
            return res.status(400).json({ message: " " + err });
          });
      }
    });
};

//List Movies
exports.listMovies = async (req, res) => {
  if (req.query.diretor) {
    const filme = Filme.find({ diretor: req.query.diretor })
      .exec()
      .then((result) => {
        return res.status(200).send(result);
      })
      .catch((err) => {
        console.log(err);
      });
  }
  if (req.query.nome) {
    const filme = Filme.find({ nome: req.query.nome })
      .exec()
      .then((result) => {
        return res.status(200).send(result);
      })
      .catch((err) => {
        console.log(err);
      });
  }

  if (req.query.genero) {
    const filme = Filme.find({ genero: req.query.genero })
      .exec()
      .then((result) => {
        return res.status(200).send(result);
      })
      .catch((err) => {
        console.log(err);
      });
  }

  if (req.query.atores) {
    const filme = Filme.find({ atores: req.query.atores })
      .exec()
      .then((result) => {
        return res.status(200).send(result);
      })
      .catch((err) => {
        console.log(err);
      });
  }
};

//Update Movie
exports.updateMovie = async (req, res) => {
  if (req.session.role !== role.ADMIN) {
    return res.status(401).send({ menssage: "401 Unauthorized" });
  } else {
    // Validate empty fields
    if (!req.params.id || !req.body) {
      return res
        .status(400)
        .send({ message: "The fields is empty, please, fill it!" });
    }

    await Filme.findOneAndUpdate(req.params.id, req.body, { new: true })
      .then((result) => {
        return res.status(200).send(result);
      })
      .catch((err) => {
        return res.status(400).send(err.message);
      });
  }
};

//Movies Details
exports.detailsMovie = async (req, res) => {
  if (req.session.role !== role.ADMIN) {
    return res.status(401).send({ menssage: "401 Unauthorized" });
  } else {
    const movie = Filme.findOne({ _id: req.params.id })
      .exec()
      .then((result) => {
        const votes = Vote.find({ filme: result._id })
          .exec()
          .then((votes) => {
            const valor = avarageResult(votes);
            if (valor) {
              return res
                .status(200)
                .send({ movie: result, Rate_Movie_Average: valor });
            } else {
              return res
                .status(200)
                .send({ movie: result, Rate_Movie_Average: 0 });
            }
          })
          .catch((err) => {
            return res.status(400).json({ message: " " + err });
          });
      })
      .catch((err) => {
        return res.status(400).json({ message: " " + err });
      });
  }
};

// Delete a Movie by id
exports.deleteMovie = async (req, res) => {
  if (req.session.role !== role.ADMIN) {
    return res.status(401).send({ menssage: "401 Unauthorized" });
  } else {
    await Filme.findByIdAndDelete({ _id: req.params.id })
      .then(() => {
        console.log("Movie was DELETE Successfully!");
        return res
          .status(200)
          .send({ message: "Movie was DELETE Successfully!" });
      })
      .catch((err) => {
        return res.status(500).json({
          error: err,
        });
      });
  }
};

//Create/Register a Vote to a Movie
exports.voteMovie = async (req, res) => {
  if (req.session.role !== role.ADMIN) {
    return res.status(401).send({ menssage: "401 Unauthorized" });
  } else {
    if (req.body.movieNote >= 0 && req.body.movieNote <= 4) {
      const vote = new Vote(req.body);
      vote
        .save()
        .then((vt) => {
          console.log("Vote Registered!");
          return res.status(201).send(vt);
        })
        .catch((err) => {
          res.status(400).json({ message: " " + err });
        });
    } else {
      return res.status(400).json({
        message: "The Movie Vote must to be equal to 0 or less then 4!",
      });
    }
  }
};

//List Votes
exports.listVotes = async (req, res) => {
  const votes = await Vote.find({});
  return res.status(200).send(votes);
};

//Update User
exports.updateUser = async (req, res) => {
  // Validate empty fields
  if (!req.params.id) {
    return res
      .status(400)
      .send({ message: "The fields is empty, please, fill it!" });
  }

  await User.findOneAndUpdate(req.params.id, req.body, { new: true })
    .then((result) => {
      return res.status(200).send(result);
    })
    .catch((err) => {
      return res.status(400).send(err.message);
    });
};
